## General requirements
1. Application code must be formatted in a consistent style and comply with the Java Code Convention.
2. If the application contains console menus or Input / Output, then they should be minimal, sufficient and intuitive. Language - English.
3. In the comments for the class, write your name and task number. Add meaningful comments to your code whenever possible.
4. Java Collection Framework (except java.util.Arrays) and regular expressions cannot be used

## The task
Write a calculator - a program that receives two numbers as input, performs an arithmetic operation ("+" - addition, "-" - subtraction, "*" - multiplication, "/" - division) and outputs the result to the console. Use a separate method for each operation. Provide a user menu to select the required operation. The length of the mantissa (the number of decimal places) is passed through command line arguments. Enter input data from the keyboard.

## Input / Output example
```
Enter the first number:
3
Enter the second number:
2
Enter operator (+, -, *, /):
/
Result: 1.5
Do you want to continue? (Y/N)
N
Bye!
```